import requests
import json
import osf

class AccessTokenSession(requests.Session):

    def __init__(self, auth_token, *args, **kwargs):
        self.auth_token = auth_token
        super().__init__(*args, **kwargs)

    @property
    def root(self):
        return('https://api.osf.io/v2')

    @property
    def auth_header(self):
        return({'Authorization': 'Bearer ' + self.auth_token})

    def get(self, *args, **kwargs):
        if 'headers' in kwargs:
            kwargs['headers'].update(self.auth_header)
        else:
            kwargs['headers'] = self.auth_header
        return(super().get(*args, **kwargs))

    def put(self, *args, **kwargs):
        if 'headers' in kwargs:
            kwargs['headers'].update(self.auth_header)
        else:
            kwargs['headers'] = self.auth_header
        return(super().put(*args, **kwargs))

    def post(self, *args, **kwargs):
        if 'headers' in kwargs:
            kwargs['headers'].update(self.auth_header)
        else:
            kwargs['headers'] = self.auth_header
        return(super().post(*args, **kwargs))

    def patch(self, *args, **kwargs):
        if 'headers' in kwargs:
            kwargs['headers'].update(self.auth_header)
        else:
            kwargs['headers'] = self.auth_header
        return(super().patch(*args, **kwargs))

    def delete(self, *args, **kwargs):
        if 'headers' in kwargs:
            kwargs['headers'].update(self.auth_header)
        else:
            kwargs['headers'] = self.auth_header
        return(super().delete(*args, **kwargs))


    def get_all(self, *args, **kwargs):
        req = self.get(*args, **kwargs)
        j = req.json()
        result = j['data']
        while j['links']['next']:
            req = self.get(j['links']['next'])
            j = req.json()
            result.extend(j['data'])
        return(result)

    def get_user(self):
        req = self.get(self.root + '/users/me/')
        return(osf.User(req.json()['data'], session=self))
