from .session import AccessTokenSession
from .user import User
from .project import Project

def session(auth_token=None):
    if auth_token:
        return(AccessTokenSession(auth_token))
    raise Exception("We have not developed session options that you are looking for yet - only the access token method is currently available")
