import osf
import googleapps

@googleapps.jsonbase
class Project(googleapps.JsonBase):
    json_properties = ['attributes', 'id', 'links', 'relationships', 'type']

    def __init__(self, jref, session):
        self.session = session
        self.process_json(jref)
        self._children = None
        self._wikis = None

    @property
    def title(self):
        return(self.attributes['title'])

    @property
    def project_type(self):
        return(self.attributes['category'])

    def _follow_relationship(self, name, *paths):
        ref = self.json['relationships'][name]
        for p in paths:
            ref = ref[p]
        data = self.session.get_all(ref['href'])
        return(data)

    @property
    def children(self):
        if not self._children:
            self._children = self._follow_relationship('children', 'links', 'related')
        return(self._children)

    @property
    def wikis(self):
        if not self._wikis:
            self._wikis = self._follow_relationship('wikis', 'links', 'related')
        return(self._wikis)
