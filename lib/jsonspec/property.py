class SchemaProperty(object):
    """ This generates a property that meets the JSON Schema specification, allowing for extra metadata at the property level.
    """

    def __init__(self, property, definition):

        self.name = property
        self.attr = '_' + property
        self._definition = definition

    @property
    def definition(self):
        return(self._definition)

    @property
    def changed(self):
        return(self._changed)

    @changed.setter
    def changed(self, value):
        self._changed = value


    def __get__(self, obj, cls=None):
        #-- if called in class context, return the property itself
        if obj is None:
            return(self)

        if not hasattr(obj, self.attr):
            # This potentially signifies that the object has not been defined and/or was not created from a fetched object.  
            # TODO: handle this more robustly
            pass
        return(getattr(obj, self.attr))

    def __set__(self, obj, value):
        if self.readonly:
            raise Exception("Cannot set read-only property " + self.name)
        if hasattr(obj, self.attr):
            if value == getattr(obj, self.attr):
                # No actual change
                return

        result = super().__set__(obj, value)
        # record that it has been set, but not committed
        self.changed = True
        obj._record_value_change(self.name)
        return(result)

    def __delete__(self, obj):
        # TODO: Delete for a property is complicated.  Not realy sure what to do here.

        result = super().__delete__(obj)
        self.changed = True
        obj._record_value_change(self.name, cleared=True)
        return( result )


def dynamic_attribute(attribute_name):
    ''' Create the attributes for the Property Class. These are defined based on the JSON Schema specification and details about the property - and we may use them in the future for validation of data input or output.  
    '''
    def prop_getter(cls):
        if attribute_name not in cls.definition:
            return
        return(cls.definition[attribute_name])
    return(property(prop_getter))

for attribute_name in ('format', 'title', 'description', 'default', 'multiple_of', 'maximum', 'exclusive_maximum', 'minimum', 'exclusive_minimum', 'max_length', 'min_length', 'pattern', 'max_items', 'min_items', 'unique_items', 'max_properties', 'min_properties', 'required', 'enum', 'type'):
    setattr(SchemaProperty, attribute_name, dynamic_attribute(attribute_name))
    

