import json
import re
from .base import DictObject
from .schema import SchemaFactory

def load(package, path):
    import googleapps
    with open(path, 'r') as fh:
        spec = googleapps.unCamel(json.load(fh))
    package['api'] =DictObject(spec)
    #package['version'] =spec['swagger']
    #package['session'] =Session(__package__, spec)
    #package['info'] =DictObject(spec['info'])

    try:
        load_actions(package, spec['paths'])    
    except Exception as e:
        import pdb,sys,traceback
        extype, value, tb = sys.exc_info()
        traceback.print_exc()
        pdb.post_mortem(tb)

def load_actions(package, spec):

    actions, schemas = parse_spec(spec)
    #package['action'] = ActionFactory(actions, package['session'])
    package['schema'] = SchemaFactory(schemas)


def parse_spec(spec):
    firstre = re.compile(r'/(\{?)([^\/]+)')
    varre = re.compile(r'\{\s*(\w+)\s*\}')
    action = {}
    all_schemas = {}
    for key in spec:      
        # parse the key into its resource and components
        # 
        if key == '/':
            # root calll special case
            # TODO: Allow for this
            continue 
        first = firstre.match(key)
        if not first:
            raise Exception("cannot parse path " + key)
        if first.group(1):
            raise Exception("first is a direct variable call: " + key)
        resource = first.group(2)
        action.setdefault(resource, {})
        action[resource][key] = spec[key]

        # go into the ref and identify the schemas
        #
        for method in spec[key]:
            for resp, ref in spec[key][method]['responses'].items():
                if not 'schema' in ref:
                    continue
                schema = ref['schema']
                if schema['type'] == 'array':
                    schemadef = schema['items']
                elif schema['type'] == 'object':
                    schemadef = schema
                else:
                    continue #?
                if schemadef['title'] in all_schemas:
                    # -- uh oh. Collision.
                    # TODO: Resolve
                    print("collision for " + schemadef['title'] )
                else:
                    all_schemas[schemadef['title']] = schemadef
    return(action, all_schemas)