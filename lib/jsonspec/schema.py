import warnings 
import re
from .property import SchemaProperty

class SchemaMeta(type):
    loaded_classes = {}
    property_class = SchemaProperty

    def __init__(klass, classname, bases, specification):
        """ During dynamic class construction, attach an api object for the Google API specification and also call __renderapi__, which is required for googleapps.meta.* classes 
        
        Args:
            klass (SchemaMeta): The schema class that has just been created.
            classname (str): The CamelCase proper name of the schema class.
            bases (tuple): The inherited parent classe(s).
            specification (dict): A dictionary of parameters. Expected to have the following:
                'api' (dict): The JSON Schema definition, as translated by the python json module
                'session' (deprecated, optional): a session object. An artifact of prior implementations, and likely to be removed.
        """
        klass._meta = SchemaClassInfo(specification['api'], klass)
        if 'session' in specification:
            klass._session = specification['session']        
        else:
            klass._session = None
        klass.__renderapi__()


    @property
    def meta(klass):
        # -- this is the meta object that contains the original json schema details and other configuration information.
        return(klass._meta)

    def __renderapi__(klass):
        for key, propdef in klass.meta.api_properties.items():
            klass.add_property(key, propdef)

    def add_property(klass, schema_property, property_definition):
        if schema_property[0] == '$':
            # This references a different schema object.
            #TODO: Actually figure out how to implement this well.
            print("-- Excluding property that references a different schema: " + schema_property)
            return

        # -- special case - if the property_name is already an attribute, (e.g., if it is 'meta')
        # it conflicts with the accessor for the actual meta object, 
        # so we rename it to meta property and throw a warning
        property_name = schema_property
        if hasattr(klass, schema_property):            
            property_name = schema_property + '_property'
            if hasattr(klass, property_name):
                raise Exception(f"Schema '{klass.meta.title}' defines property '{schema_property}', which is already a class function, and also {property_name}. This is unexpected and weird")
            warnings.warn(f"Schema '{klass.meta.title}' defines property '{schema_property}', which is already a class function. Redefining it to {property_name}")
        
        property_class = klass.property_class
        property = property_class(property_name, property_definition)
        setattr(klass, property_name, property)        
        klass.meta.property_map[property_name] = schema_property

class SchemaClassInfo():
    def __init__(self, api, source):
        self.api = api
        self.source = source
        self.property_map = {}
    
    @property
    def title(self):
        return(self.api['title'])
    
    @property
    def type(self):
        return(self.api['type'])
    
    @property
    def format(self):
        return(self.api['format'])

    @property
    def properties(self):
        return(sorted(self.property_map))

    @property
    def api_properties(self):
        return(self.api['properties'])

    @property
    def additional_properties(self):
        return(self.api['additional_properties'])

class Schema():    
    def _record_value_changed(self, property):
        print(f"Recording that {property} was changed")

import sys
sys.path.insert(0, '../dev/datacleaner')
import datacleaner.camelcase
camel = datacleaner.camelcase.CamelCase(initial_upper=True)


class SchemaFactory():
    def __init__(self, object_model, session=None):
        for resource, rdef in object_model.items():
            resource_name = camel.clean(resource)    
            setattr(self, resource_name, schema_class_factory(resource, resource_name, rdef, session))
    


def schema_class_factory(resource, classname, apidef, session):
    print("MODEL FACTORY:", classname)
    klass = SchemaMeta(classname, (Schema,), {'resource': resource, 'api': apidef, 'session': session})
    print("Produced:", klass.__name__)
    return(klass)
