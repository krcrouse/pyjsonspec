

class ActionMeta(type):
    loaded_classes = {}

    def __init__(klass, classname, bases, specification):
        """ During dynamic class construction, attach an api object for the Google API specification and also call __renderapi__, which is required for googleapps.meta.* classes """
        klass.resource = specification['resource']
        klass.api = specification['api']        
        klass.session = specification['session']

    def __renderapi__(klass):

        klass.schema_json = klass.api.definition['schemas'][klass.api_schema]
        #klass.add_properties( klass.schema_json['properties'] )

        metaproperties = googleapps.API('discovery', 'v1').definition['schemas']['JsonSchema']
        #property_class = generate_metapropertyclass(metaproperties)
        from .property import RemoteProperty
        klass.add_properties( klass.schema_json['properties'],  property_class=RemoteProperty)

    def add_properties(cls, definition, exclude=None, rename=None, alias=None, recurse=False, property_class=None):

        normalized = googleapps.unCamel(definition)
        for key, cdef in normalized.items():

            if re.match(r'\$', key):
                # This references a different schema object.
                #TODO: Actually figure out how to implement this well.
                print("-- Excluding property that references a different schema: " + key)
                continue

            #
            # Determine the property name
            #
            if exclude and key in exclude:
                print("-- Excluding " + key)
                continue
            if rename and key in rename:
                propname = rename[key]
            else:
                propname = key

            if hasattr(cls, propname):
                raise Exception("Attempting to add a property '" + str(key) + "' to class " + cls.__name__ + ", schema but this class already has a property by that name.  You will need to rename this property in the __renderapi__ call")

            if property_class is None:
                from .property import RemoteProperty
                property_class = RemoteProperty
            property = property_class(propname, cdef)
            setattr(cls, propname, property)
            cls.__properties__.append(property)

            if alias and propname in alias:
                # ALSO make a reference to the same property here
                print("Linking alias for " + alias[propname])
                # But note the alias doesnt show up in __properties__, just __property_aliases__
                setattr(cls, alias[propname], property)

            if alias:
                cls.__property_aliases__.update(alias)


class ActionClass(type):
    
    @classmethod
    def list(self, **kwargs):
        """ translates /function/ to list """
        path = '/' + self.resource + '/'
        spec = self.api[path]

        import pdb 
        pdb.set_trace()
        req = self.session.get(path)
        data =req.json()['data']
        return(data)


import sys
sys.path.insert(0, '../dev/datacleaner')
import datacleaner.camelcase
camel = datacleaner.camelcase.CamelCase(initial_upper=True)


class ActionFactory():
    def __init__(self, object_action, session):
        for resource, rdef in object_action.items():
            resource_name = camel.clean(resource)    
            setattr(self, resource_name, action_class_factory(resource, resource_name, rdef, session))
    


def action_class_factory(resource, classname, apidef, session):
    print("ACTION FACTORY:", classname)
    klass = ActionMeta(classname, (ActionClass,), {'resource': resource, 'api': apidef, 'session': session})
    print("Produced:", klass.__name__)
    return(klass)
