import os,sys,re
import traceback, pdb, warnings
from pprint import pprint
#sys.path.insert(0, '../stable/googleapps')
import googleapps
import googleapps.docs

sys.path.append('lib')
import osf

token = 'Rz3v76Gv3AGkvfkYDmt1ZU2DuvWneVwuC1NBSOMW9tnEKch5BUOV8Ct3HPof1F7rRlzm1U'

def main():
    try:
        run()
    except Exception as e:
        extype, value, tb = sys.exc_info()
        traceback.print_exc()
        pdb.post_mortem(tb)

def run_api():
    import openapi
    import json
    with open('conf/osf-api.v2.2020-07-17.json', 'r') as fh:
        spec = json.load(fh)
    #pprint(spec)
    osfapi = openapi.Swagger(spec)
    import pdb
    pdb.set_trace()
    osfapi.authenticate('personalAccessToken', token)
    stuff = osfapi.call_users_me()


def run():
    session = osf.session(token)
    user = session.get_user()
    pprint(user.json)
    #- -- get project lists
    #projects = user.projects
    #for p in projects:
    #    print("--", p.title, p.id, p.type, p.project_type)

#    testp = user.get_project('p2kds')
#    pprint(testp.json)
#    for c in testp.children:
#        print('+++')
#        pprint(c.json)

#    for w in testp.wikis:
#        print('+++')
#        pprint(w.json)
    testp_id = 'p2kds'

    doc = parse_doc()
    req = session.post(session.root + '/nodes/' + testp_id + '/wikis/', data={
            'name': 'AutoExp',
            'type': 'wikis',
            'content': doc,            
    })

    print(req.status_code)
    print(req.reason)
    print(req.content)
    import pdb
    pdb.set_trace()
    pass
    #--- delete wiki


def parse_doc():
    xxx = googleapps.docs.get('1i0CnNs6xLmeuaCiauwUxiqcUo6aGVQ0BFV4E30I2XK8')
    doc = ""
    for elem in xxx.body:
        if elem.body_type == 'paragraph':
            string = ""
            if elem.paragraph_type == 'heading':
                string = '#' * elem.heading_level + ' '
            elif elem.list_type == 'ordered':
                string = '1. '
            elif elem.list_type == 'unordered':
                string = '* '
            elif elem.paragraph_type == 'title':
                string = '# '
            elements = elem.elements

            for i, para in enumerate(elements):
                if para.inline_object_element_id:
                    string = "\n[[ IMAGE OR OBJECT INSERTED HERE - NOT YET SUPPORTED BY googleapps ]]\n"
                elif para.text:
                    text = ""
                    # -- if the font is in courier/new, we consider that a code block

                    if para.text_style.link:
                        text += f'[{para.text}]({para.text_style.link})'
                    else:
                        text += para.text

                    if para.text_style.font_family and 'Courier' in para.text_style.font_family:
                        text = re.sub(r'^(\s*)(\S)', lambda m: m.group(1) + '`' + m.group(2), text)


                    if para.text_style.bold:
                        text = re.sub(r'^(\s*)(\S)', lambda m: m.group(1) + '**' + m.group(2), text)

                    if para.text_style.italic:
                        text = re.sub(r'^(\s*)(\S)', lambda m: m.group(1) + '*' + m.group(2), text)


                    # for the closing bits, we ensure that we are on the left side of a newline
                    if para.text_style.italic:
                        text = re.sub(r'(\S)(\s*)$', lambda m: m.group(1) + '*' + m.group(2), text)
                    if para.text_style.bold:
                        text = re.sub(r'(\S)(\s*)$', lambda m: m.group(1) + '**' + m.group(2), text)
                    # -- if the font is in courier/new, we consider that a code block
                    if para.text_style.font_family and 'Courier' in para.text_style.font_family:
                        text = re.sub(r'(\S)(\s*)$', lambda m: m.group(1) + '`' + m.group(2), text)

                    string += text
                elif para.element_type == 'horizontal_rule':
                    string += "---"
                elif para.element_type == 'page_break':
                    string += "\n\n[PAGE BREAK]\n\n"
                    pass

            doc += string
        elif elem.body_type == 'section break':
            print("\n\n[SECTION BREAK]\n\n")
        else:
            print(elem.json)
            print("\n\n")
#    print(doc)
    return(doc)


main()
